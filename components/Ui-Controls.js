import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity , Image, Dimensions } from 'react-native';
import  {postData} from '../helpers'
import Images from '../assets/Images'


export function ALabel(props) {
    const style = StyleSheet.create({
        label: {
            padding: 5,
            paddingLeft:6,
            paddingRight:6,
            backgroundColor: "white",
            borderWidth:1,
            borderColor:"#008a65",
            alignSelf:"center",
            marginBottom: 10,
            borderRadius:6,
            marginTop: 10
        },
        labelText:{
            color:"#008a65",
            fontSize: 25,
            textAlign:"center" 
        },
        labelContent:{
            flexDirection:"row"
        }
    })
    return (<View style={style.label}>
        <View style={style.labelContent}>
            <Text style={style.labelText}>{props.caption}</Text>
        </View>
    </View>);
}

export function AButton(props) {
    const style = StyleSheet.create({
        button: {
            padding: 5,
            backgroundColor: "#5accae",
            alignSelf: "center",
            marginBottom: 10,
            borderRadius:6,
            marginTop: 10
            
        },
        buttonText:{
            color:"white",
            fontSize: 25,
            textAlign:"center"
        },
        buttonContent:{
            flexDirection:"row"
        },
        fontAwesome:{
            fontFamily: 'fontawesome', 
            fontSize: 25 ,
            color:"white" ,
            paddingRight:5,
            paddingLeft:5, 
            paddingTop:3,
            paddingBottom:3
        }
    })

    return (
    <View>
        <TouchableOpacity activeOpacity={0.8}  onPress={props.onPress} style={style.button}>
            <View style={style.buttonContent}>
                <Text style={style.fontAwesome}>{typeof props.fa != 'undefined' ? props.fa : ''}</Text>
                <Text style={style.buttonText}>{typeof props.caption !== 'undefined' ? props.caption : ''}</Text>
            </View>
        </TouchableOpacity>
    </View>);
    
}

export function ImageSummary(props) {

    var width = Dimensions.get('window').width; //full width
    
    const style = StyleSheet.create({
        imageSummary: {
            width: width,
            height: 250,
        },
        spacing:{
            marginTop:20
        }
    })
    console.log(props.image)
    let imageSource = Images[props.image]
    if(imageSource === undefined)
    {
        imageSource = Images.drawing
    }
    console.log(imageSource)

    return (<View style={style.spacing}><Image style={style.imageSummary} alt={`afbeelding van ${props.name}`} source={imageSource} /></View>
    );
}

export function ImageDetail(props) {
    const style = StyleSheet.create({
        imageDetail: {
            width: 220,
            height:180,
            display: "flex",
            justifyContent: "center",
            alignItems: "center"
        }
    })
    let imageSource = Images[props.image]
    if(imageSource === undefined)
    {
        imageSource = Images.drawing
    }

    return (<Image style={style.imageDetail} alt={`afbeelding van ${props.name}`} source={imageSource} />);
}

export function UnorderedList(props) {
    return (<UnorderedList>
        {props.ul.map(item => (<li key={item.key}>{item.key}</li>))}
    </UnorderedList>);
}

const likeSpacing = StyleSheet.create({
    container:{
        flexDirection:"row",
        justifyContent:"flex-start",
        marginLeft:15
    },
    item:{
        paddingRight:10
    }
})

export class LikePanel extends React.Component {
    
    host = "http://vps-d3e01a17.vps.ovh.net/MmtLike";
    //De get wordt uitgevoerd als het keyvalue attribuut gedefinieerd is. Zo blijft onze oude code nog werken
    getLikes = () => {
        const keyValue = this.props.keyValue;
        let count = 0;
        console.log(keyValue)
        if (typeof keyValue !== 'undefined') {
            // als de callback wordt uitgevoerd, is this niet meer in de scope
            // daarom slaan we die op in een constante en geven die met de callback mee 

            const self = this;
            const url = `${this.host}/${keyValue}`;
            console.log("dit is de url" + url)
            fetch(url)
                .then(response => response.json())
                .then(data => {
                    this.setState({
                        likes: data.likes
                        //Hier halen we de likes op en geven ze mee aan de variabele likes die we achteraf gebruiken
                    });
                }).then(item => console.log(item));
        }
    }
    //De volgende functie gaat de likes in de databank posten
    postLikes = () => {
        let item = {
            Key: this.props.keyValue, //Het geeft de key van het huidige item mee om de likes bij het juiste item toe te voegen
            Name: 'onbelangrijk',
            Likes: 1 //Het geeft 1 like
        };

        postData(this.host, item) //We roepen de locatie van de API op en ook het item met de like en key
            .then(data => {
                this.setState({
                    likes: data.likes //Het zet de likes gelijk aan de likes van de huidige pagina
                });
            });
    }

    constructor(props) {
        // meegeven van props aan super noodzakelijk is, omdat het de constructor van React.Component in staat stelt this.props te initialiseren
        // 🔴 Kan 'this' nog niet gebruiken.
        super(props);
        // ✅ Nu kan het wel.
        this.state = {
            likes: 0
        };
        console.log("constructor" + this.props.keyValue)
        this.getLikes(); //Deze functie zal de likes gelijk zetten aan de likes van het huidige item
    }

    incrementLike = () => {
        const keyValue = this.props.keyValue; //We zetten de keyvalue gelijk aan het huidige key item
        if (typeof keyValue !== 'undefined') { //Als het keyvalue gekend is dan posten we de likes
            this.postLikes();
            //Als het niet gekend is dan verhogen we het gewoon op het scherm maar posten we niets. Zodanig blijft de legacy code werken
        } else {
            let newCount = this.state.likes + 1;
            this.setState({
                likes: newCount
            });
        }
    };

    render() {
        return (
            <View style={likeSpacing.container}>
                <View style={likeSpacing.item}>
                    <AButton fa="&#xf164;" caption="Like" onPress={this.incrementLike} />
                </View>
                <View>
                    <ALabel caption={this.state.likes} />
                </View>
            </View>);
    }
}
