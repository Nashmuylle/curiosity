import React from 'react';
import {StyleSheet, ScrollView} from 'react-native';
import {ImageSummary, AButton} from './Ui-Controls';

export class ArticleSummary extends React.Component {
  style = StyleSheet.create({
    articleSummary: {
      display: 'flex',
      flexDirection: 'row',
      flexWrap: 'wrap',
      justifyContent: 'center',
    },
  });

  showDetail = () => {
    this.props.action(this.props.item);
  };

  render() {
    return (
      <ScrollView key={this.props.item.key}>
        <ImageSummary
          name={this.props.item.name}
          image={`${this.props.item.image}`}
        />
        <AButton caption={this.props.item.name} onPress={this.showDetail} />
      </ScrollView>
    );
  }
}
