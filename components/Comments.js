import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity , Image, TextInput, Button } from 'react-native';
import {AButton} from './Ui-Controls'
import {ALabel} from './Ui-Controls'
import  {postData} from '../helpers'

function AShowComment (props) {
  const style = {
    flexStyle:{
      flexDirection: "row",
      justifyContent:"flex-start",
    },
    commentArea:{
      padding: "1%",
      backgroundColor: "white",
      borderWidth:1,
      borderColor:"#008a65",
      alignSelf:"flex-start",
      marginBottom: 15,
      marginLeft: 10,
      marginRight: 50,
      paddingBottom:10,
      borderRadius:6,
    },
    commentAreaTitle:{
      color:"#008a65",
      fontSize: 20,
      fontWeight:"bold"
    },
    text:{
      color:"#008a65",
      fontSize: 20,
      textAlign:"left" 
    },
    image:{
      width: 50,
      height: 50,
      borderRadius:10,
    },
    title:{
      paddingBottom:20,
      fontFamily:"Raleway-Light",
      fontSize: 25,
      fontWeight: "bold",
      color: "#5accae",
    },
    titlePlacing:{
      alignItems:"flex-start",
      margin:"5%",
    },
  }
    console.log(props)
    return(
    <View style={style.titlePlacing}>
      <Text style={style.title}>Comments {props.comments.length}</Text>
      {props.comments.map(item => (
        <View key={item.id} style={style.flexStyle}>
        <View>
        <Image style={style.image} source={{uri: `https://api.adorable.io/avatars/285/${item.name}`}}/>
          </View>
      <View style={style.commentArea}>
          <Text style={style.text}>Naam: {item.name}</Text>
          <Text style={style.text}>Comment: {item.comment}</Text>
      </View>
    </View>)
    )}
    </View>)
  }
  
  export class CommentPanel extends React.Component{
    host = "http://vps-d3e01a17.vps.ovh.net/MmtComment";

    style = StyleSheet.create({
        panelStyle: {
          padding: 10,
          marginLeft:15,
          marginRight:15,
          borderWidth:1,
          borderColor:"gray",
          borderRadius: 6,
          fontSize:20,
          textAlign:"left"
        }
  })
  
    constructor(props){
      super(props)
      this.state={
        name: "", 
        comment: "",
        comments: [] //We gaan telkens de huidige comment en naam opslaan in een array zodat we deze mooi kunnen tonen 
      }
    }
    
    //De volgende 2 functies halen de waarde op van de input en steken deze in de state
    handleNameChange = (name) => this.setState({ name });
    handleCommentChange = (comment) => this.setState({ comment });

    submit = () => {
        const {name} = this.state
        const {comment} = this.state
        const {comments} = this.state.comments.push({name: this.state.name, comment: this.state.comment})
        //We vullen de comments in met de huidige comment en naam
        if (this.state.comments) {
          this.setState({ comments: this.state.comments });
          this.postComments(name, comment)
          
        } else {
          alert('Please enter your comment first');
        }
      };

      postComments = (name, comment) => {
        //console.log(data)
        let item = {
            Key: this.props.keyValue,
            name: name,
            comment: comment
        };

        postData(this.host, item)
    }

    async componentDidMount() {
      const keyValue = this.props.keyValue;
      const self = this;
      const url = `${this.host}/${keyValue}`;
      const response = await fetch(url); //The keyword await makes JavaScript wait until that promise settles and returns its result.
      const json = await response.json();
      //console.log(json)
      this.setState({ comments: json })
      //console.log("dit is de comments")
      //console.log(this.state.comments)
  }


      render () {
        return(
          <View>
              <View >
                <ALabel caption="Jouw commentaar"></ALabel>
              </View>
              <View>
                <TextInput style={this.style.panelStyle} placeholder="Naam"  name="fullName" onChangeText={this.handleNameChange}  ></TextInput>
                <TextInput style={this.style.panelStyle} placeholder="Comment" name="comment" onChangeText={this.handleCommentChange}  ></TextInput>
              </View>
              <View style={{marginTop:10}}>
                <View >
                  <AButton fa="&#xf1d8;" caption="Verzenden" onPress={this.submit}/>
                </View>
              <AShowComment comments={this.state.comments}></AShowComment>
            </View>
        </View>
        )
      }
  
  }
