import React from 'react';
import { StyleSheet, Text, View, Image, SafeAreaView, ScrollView, FlatList } from 'react-native';
import { ArticleDetail } from './ArticleDetail'
import {ArticleOverview} from './ArticleOverview'


export class ArticleApp extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            overview: true
        };
    }

    showDetail = (item) => {
        this.setState({
            overview: false,
            article: item
        });
    };

    showOverview = () => {
        this.setState({
            overview: true,
            article: Text
        });
    };

    render() {

        if (this.state.overview) {
            return (
                <ArticleOverview article={this.props.article} action={this.showDetail} />
            );
        } else {
            return (<ArticleDetail article={this.state.article} actionOverview={this.showOverview} />)
        }
    }
}

