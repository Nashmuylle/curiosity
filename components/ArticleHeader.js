import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import Images from '../assets/Images'

export default function ArticleHeader(props) {

    const style = StyleSheet.create({
        header: {
            alignItems: "center",
        },
        title: {
            fontFamily:"Raleway-Light",
            fontSize: 30,
            fontWeight: "bold",
            color: "#5accae",
            marginBottom: 14,
        },
        logo: {
            marginTop: 15,
            marginBottom: 32,
            width: 180,
            height: 140
        }
    });

    return (
        <View style={style.header}>
            <Image source={Images.logo} style={style.logo} />
            <Text style={style.title}>{props.title}</Text>
        </View>);
}

