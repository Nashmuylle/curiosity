import React from 'react';
import {StyleSheet, ScrollView} from 'react-native';
import { ArticleSummary} from './ArticleSummary'

export function ArticleOverview(props) {
  const style = StyleSheet.create({
    articleOverview: {
      color: 'white',
      display: 'flex',
      flex: 1,
      flexWrap: 'wrap',
      justifyContent: 'center',
      alignItems: 'center',
    },
    scrollView: {},
  });
  // alert(JSON.stringify(props));
  return (
    <ScrollView style={style.scrollView}>
      {props.article.map(item => (
        <ArticleSummary item={item} key={item.key} action={props.action} />
      ))}
    </ScrollView>
  );
}
