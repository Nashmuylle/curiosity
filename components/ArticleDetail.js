import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { AButton, ImageDetail, LikePanel } from './Ui-Controls'
import { CommentPanel } from './Comments'

export class ArticleDetail extends React.Component {
    style = StyleSheet.create({
        detail: {
            display: "flex",
            flexDirection: "column",
            flex: 1
        },
        top: {
            alignItems:"center"
        },
        middle: {
            alignItems:"flex-start",
            marginTop:10,
            marginBottom: 10,
            paddingLeft:40,
        },
        text:{
            fontSize:18,
            fontFamily:"Raleway-Regular",
            color:"black",
        },
        textGreen:{
            color:"#5accae",
            fontSize:20,
            fontFamily:"Raleway-ExtraBold",
        },
        spacing:{
            flexDirection:"row",
            marginLeft:10
        }
    })

    constructor(props) {
        super(props);
        console.log(this.props.article.key)
    }

    showOverview = () => { this.props.actionOverview() }
    

    render() {
        return (<View >
            <View style={this.style.spacing}>
                <AButton fa="&#xf104;" onPress={this.showOverview} />
            </View>
            <View style={this.style.top}>
                <ImageDetail name={this.props.article.name} image={`${this.props.article.image}`} />
            </View>
            <View style={this.style.middle}>
                <Text style={this.style.text}><Text style={this.style.textGreen}>Naam:</Text>  {this.props.article.name}</Text>
                <Text style={this.style.text}><Text>Type:</Text>  {this.props.article.type}</Text>
                <Text style={this.style.text}><Text style={this.style.textGreen}>Periode:</Text> {this.props.article.period}</Text>
                <Text style={this.style.text}><Text>Land:</Text> {this.props.article.country}</Text>
                <Text style={this.style.text}><Text style={this.style.textGreen}>Stad:</Text> {this.props.article.city}</Text>
                <Text style={this.style.text}><Text>Coordinaten:</Text> {this.props.article.coordinates}</Text>
            </View>
            <View>
                <LikePanel keyValue={this.props.article.key}></LikePanel>
                <CommentPanel keyValue={this.props.article.key}/>
            </View>
        </View>);
    }
}
