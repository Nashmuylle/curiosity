const images = {
    logo: require("./img/mmt-logo.png"),
    drawing: require("./img/dolmen-drawing.png"),
    AlleecouverteduColombier: require("./img/big/Allee-couverte-du-Colombier.jpg"),
AlleecouverteduColombier: require("./img/big/Allee-couverte-du-Colombier.png"),
DolmendAmenon: require("./img/big/Dolmen-d-Amenon.jpg"),
DolmendelaPierre: require("./img/big/Dolmen-de-la-Pierre.png"),
DolmendeLhomme: require("./img/big/Dolmen-de-Lhomme.jpg"),
dolmendrawing: require("./img/big/dolmen-drawing.png"),
Lespierrestournantes: require("./img/big/Les-pierres-tournantes.jpg"),
MenhiranciendolmendelaPierre: require("./img/big/Menhir--ancien-dolmen--de-la-Pierre.jpg"),
MenhirdeCourtevrais: require("./img/big/Menhir-de-Courtevrais.jpg"),
MenhirdeGobianne: require("./img/big/Menhir-de-Gobianne.jpg"),
MenhirdelaBrunerie: require("./img/big/Menhir-de-la-Brunerie.jpg"),
MenhirdeLorriere: require("./img/big/Menhir-de-Lorriere.jpg"),
MenhirditMenhirduGue: require("./img/big/Menhir-dit-Menhir-du-Gue.jpg"),
MenhirduCaillou: require("./img/big/Menhir-du-Caillou.png"),
MenhirduPerray: require("./img/big/Menhir-du-Perray.png"),
Menhirnonleve: require("./img/big/Menhir-non-leve.jpg"),
menhirdrawing: require("./img/big/menhir-drawing.jpg"),
Menhir: require("./img/big/Menhir.jpg"),
MenhirsdelaMereetdelaFille: require("./img/big/Menhirs-de-la-Mere-et-de-la-Fille.jpg"),
NecropoleneolithiquedutheatreantiquedeCherre: require("./img/big/Necropole-neolithique-du-theatre-antique-de-Cherre.png"),
PaletdeGargantua: require("./img/big/Palet-de-Gargantua.jpg"),
Pierrecouverte2: require("./img/big/Pierre-couverte-2.jpg"),
Pierrecouverte: require("./img/big/Pierre-couverte.jpg"),
PierreFiche: require("./img/big/Pierre-Fiche.jpg"),
PierrePotelee: require("./img/big/Pierre-Potelee.jpg"),
PierreSaintJulien: require("./img/big/Pierre-Saint-Julien.jpg")
    


};


export default images;