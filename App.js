/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Dimensions
} from 'react-native';

import ArticleHeader from './components/ArticleHeader';
import { ArticleApp } from './components/Ui-Article';
import * as data from './data/neolithicum.json';
export default class App extends React.Component {


  width = Dimensions.get('window').width;
  render() {
    return (
      <SafeAreaView>
        <View style={this.style.order}>
          <ScrollView >
            <ArticleHeader title={data.title} />
            <ArticleApp article={data.curiosity} />
          </ScrollView>
        </View>
      </SafeAreaView>
    );
  }
  style = StyleSheet.create({
    order: {
      width: this.width


    }
  })
};
